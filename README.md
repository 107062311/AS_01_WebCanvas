# 2020 軟體實驗設計
## Assignment 01 Web Canvas


[TOC]

---

這是我的Canvas預覽：
![](https://i.imgur.com/aIdzwRo.jpg)

---

## How to use 

* **Pen**
    一進入網站就可以開始畫畫喔！在切換其他功能之後，需要重新點選網頁中的畫筆圖片，網站小精靈才會知道現在該畫什麼喔！
    ![](https://i.imgur.com/zVFjXEl.jpg)

    如果想要讓世界變得繽紛，可以點選左手邊的Color，根據自己現在的心情選個可愛的顏色吧！
    ![](https://i.imgur.com/JbwJ3jF.jpg)

    要是太細看不清楚，可以調整畫筆的粗細喔！移動滑軌，調成合適的大小吧！
    ![](https://i.imgur.com/reFFLPK.jpg)

* **Eraser**
    要是出了點意外，想要把多畫的東西擦掉，這時候就是橡皮擦登場的時機了！點選橡皮擦，可以把畫筆的軌跡給擦乾淨喔！
    ![](https://i.imgur.com/3ZMEYf5.jpg)
    要是太細擦太慢，也可以改變Size，加速一下！
    ![](https://i.imgur.com/reFFLPK.jpg)

* **Text**
    如果想要寫點文字，點一下文字圖示，就可以把內心所有想說的話都畫在畫布上囉。以下是小小使用教學：
    首先，我們在文字框中輸入文字。
    ![](https://i.imgur.com/DGoJMfn.jpg)
    再點選圖示
    ![](https://i.imgur.com/j64gZpt.jpg)
    就可以在畫布上鼠標得位置畫出來囉！
    
    若是想要更改字體粗細，同樣可以將滑鼠移至Size底下來調整
    ![](https://i.imgur.com/reFFLPK.jpg)
    但要小心的是，畫布繪製的文字是以文字外框來描繪的，因此粗細的size建議不要超過10，不然會看不清楚印的文字喔！
    
    而字的大小與字型，同樣可以根據自己的喜來來更改喔，在選單內選個喜歡的吧！
    ![](https://i.imgur.com/F8pmj1v.jpg)

* **Circle**
    點選圈圈圖示，滑鼠一開始點的位置就是我們的圓心，滑鼠移動多少就會是圓半徑喔！
    ![](https://i.imgur.com/Eizrtxl.jpg)
    同樣可以調整圓的粗細。
    ![](https://i.imgur.com/reFFLPK.jpg)
    也可以改變顏色！
    ![](https://i.imgur.com/JbwJ3jF.jpg)
    
* **Rectangle**
    點選長方形圖示，一開始點的位置就是長方形左上角的點點，滑鼠移動到哪裡長與寬就會改變到哪裡喔！
    ![](https://i.imgur.com/srPWBWI.jpg)
    也可以選自己想要的顏色，
    ![](https://i.imgur.com/JbwJ3jF.jpg)
    與調整粗細！
    ![](https://i.imgur.com/reFFLPK.jpg)
    
* **Triangle**
    點一下三角形圖示，一開始的那個點點就是我們三角形的頂點！滑鼠移動的距離就是我們三角形的腰身啦！
    ![](https://i.imgur.com/ozqqkIl.jpg)

    同樣可以透過滑軌調整粗細，
    ![](https://i.imgur.com/reFFLPK.jpg)
    與更改顏色！
    ![](https://i.imgur.com/JbwJ3jF.jpg)
* **Reset**
    如果想回到當初，就點選Reset底下的垃圾桶吧，他會把所有的過去都刪掉喔！
    ![](https://i.imgur.com/7ALvtHE.jpg)

* **Undo**
    要是後悔了可以點點Undo，他會帶你回到過去一點點的時光！
    ![](https://i.imgur.com/5A44Yp2.jpg)

* **Redo**
    後悔回到過去，也還有機會回到現在喔！點選Redo，回到當下。
    ![](https://i.imgur.com/XFhRJOB.jpg)

* **Color**
    點選Color底下的框框
    ![](https://i.imgur.com/JbwJ3jF.jpg)
    就會出現選擇顏色的地方，選好自己喜歡的顏色就可以更改畫筆的顏色囉！
    ![](https://i.imgur.com/jgks166.jpg) 

* **Size**
    移動滑軌，就更改畫筆的粗細啦。
    ![](https://i.imgur.com/48PSDqg.jpg)

* **Download**
    點一下Download底下的圖片即可立即下載，檔名都是固定的喔。
    ![](https://i.imgur.com/5Pehgx3.jpg)

* **Upload**
    點一下即可Upload底下的圖片，可以自由選擇想要上傳上來圖片，圖片會遍布整張畫布喔！
    ![](https://i.imgur.com/swx66Sg.jpg)

* **Music**
    左上方有個音樂的欄位，一點進網站就會自動循環播放超～好聽的音樂，若是不想聽了可以按下暫停，也可以自由調整音量大小聲！點選音量旁的更多功能選項，還可以把好聽的音樂下載下來喔！
    ![](https://i.imgur.com/2NvJx3F.jpg)


## Function description
我的畫布上有一些主要的功能，以下我將一一介紹
* Pen
透過偵測滑鼠事件(addEventListener)，在mousedown的瞬間將現在的位置存起來，在mousemove的時候時刻更改現在的位置，並且使用「moveTo」移到上次的滑鼠位置與「lineTo」將現在的滑鼠位置與上次的滑鼠位置相連。
我將Pen功能設為預設值，也就是說，在一開始進入網頁時即可在畫布上作畫。點選畫布後即可看到鼠標更改。
當點選Pen底下的圖片時，會呼叫pen()function，更改Drawmode，以便在偵測滑鼠事件時知道現在該如何進行繪畫。Drawmode為固定的const variables，寫在整份程式的最上端，作為define的值使用。
可以透過Size的值來更改現在畫筆的粗細，Size的實作會在底下Size的地方說明。
可以透過Color來更改現在畫筆的顏色，Color的實作會在底下Color的地方說明。
* Eraser
偵測滑鼠事件的內容基本上和Pen的功能一致，但多加了globalCompositeOperation的功能，，於是可以透過設定type為destination-out實作出清除部分區域的功能。而在mouseup的時候將他的type改為source-over的預設值，繼續作畫。
點選Eraser底下的圖片時，會呼叫eraser()function，更改Drawmode。
同樣可以透過Size的值來更改現在畫筆的粗細，Size的實作會在底下Size的地方說明。

* Text
透過input text來決定現在要畫上去的文字是什麼。在點選調整字體大小與字型的選單時，會分別呼叫Font(value)與Size(value)，將當下的選好的值傳到JS中，並透過ctx.font的功能更改字體的大小與字型。
點選Text底下的圖片時，會呼叫word()function，更改Drawmode的數值。
同樣可以透過Size的值來更改現在畫筆的粗細，Size的實作會在底下Size的地方說明。
也可以透過Color來更改現在畫筆的顏色，Color的實作會在底下Color的地方說明。

* Circle
透過偵測滑鼠事件(addEventListener)來將mousedown的瞬間將現在的位置存起來，在mousemove的時候將原本的點設為圓心，使用ctx.arc來畫出圓。
點選Circle底下的圖片時，會呼叫circle()function，更改Drawmode的數值。
每次畫圖的時候，我都會在mousedown時先記住現在畫布的狀態，在畫的時候將畫布清空，再重新畫上上次儲存的狀態，以達成一次只畫一個圓的功能。
同樣可以透過Size的值來更改現在畫筆的粗細，Size的實作會在底下Size的地方說明。
可以透過Color來更改現在畫筆的顏色，Color的實作會在底下Color的地方說明。
* Triangle
透過偵測滑鼠事件(addEventListener)來將mousedown的瞬間將現在的位置存起來，在mousemove的時候將上次紀錄的點設為三角形的頂點，先moveTo上次紀錄的點，將他和現在的位置畫上連線，並根據現在的X，Y座標畫出等腰三角形的另一個腰身，再moveTo另一個頂點，將三角形完成。
每次畫圖的時候，我都會在mousedown時先記住現在畫布的狀態，在畫的時候將畫布清空，再重新畫上上次儲存的狀態，以達成一次只畫一個三角形的功能。
點選Triangle底下的圖片時，會呼叫triangle()function，更改Drawmode的數值。
同樣可以透過Size的值來更改現在畫筆的粗細，Size的實作會在底下Size的地方說明。
可以透過Color來更改現在畫筆的顏色，Color的實作會在底下Color的地方說明。
* Rectangle
同樣透過偵測滑鼠事件(addEventListener)來將mousedown的瞬間將現在的位置存起來，在mousemove的時候將上次紀錄的點設為長方形左上角的頂點，透過strokeRect來畫出以上次位置與現在滑鼠位置組合成的長方形。
每次畫圖的時候，我都會在mousedown時先記住現在畫布的狀態，在畫的時候將畫布清空，再重新畫上上次儲存的狀態，以達成一次只畫一個長方形的功能。
點選Rectangle底下的圖片時，會呼叫rectangle()function，更改Drawmode的數值。
同樣可以透過Size的值來更改現在畫筆的粗細，Size的實作會在底下Size的地方說明。
可以透過Color來更改現在畫筆的顏色，Color的實作會在底下Color的地方說明。
* Color
我Color是使用input的type為color來實作，並將顏色預設為黑色。
* Size
我的input type為range，我設定最大值為30，最小值1，可以在JS中使用DOM的getElementById，獲得現在滑軌中的值，並assign給ctx.lineWidth，即可完成畫筆粗細的實作。
* Reset(
在reset中，我透過onclick呼叫reset()，會使用ctx.clearRect來將整張圖清空。
並把redo_history與undo_history清空。
* Undo&Redo
我創造了兩個Array分別是redo_history與undo_history，來存所有畫過的圖。在mousedown的時候推進stack中，若是按了Redo，則將現在的圖push進入redo_history，並清空畫布重新畫上現在的狀態，再將undo_history pop一個；Undo則反之。
* Download
我在Download的地方開一個超連結的tag```<a></a>```，並在點選圖片時呼叫download()function，將圖片的href設成現在的圖片URL。

* Upload
我透過mouse的事件，來決定現在的target file，並使用Filereader()，將我們選好的照片load至畫布上。
* Music
使用audio 的tag，將背景音樂設定autoplay與循環播放，並設定preload，可以讓播放音樂時順利一些。


## Gitlab page link

https://107062311.gitlab.io/AS_01_WebCanvas
)
## Others(不重要)

    助教安安，助教辛苦了，要是心很累可以看GODJJ喔（前提是JJ有開台:DDD)
    JJJJ起來:DDD

<style>
table th{
    width: 100%;
}
</style>