var Canvas = document.getElementById("canvas");
var ctx = Canvas.getContext("2d");
ctx.lineJoin = "round";
ctx.lineCap = "round";
var Lastcanvas = new Image();
var Drawmode = 1;
var StartDraw = 0;
var redo_history = [];
var undo_history = [];

const Clear = 0;
const Pen = 1;
const Eraser = 2;
const Circle = 3;
const Triangle = 4;
const Rectangle = 5;
const Word = 6;

// var ctx = Canvas.getContext("2d");
var Mouse = {
    x: 0,
    y: 0
}
var Lastmouse = {
    x: 0,
    y: 0
}
var range = 0;
var fontsize = 0;
var C_range = 0;
var T_range = 0;
var F_size = 0;
var ffont;
var ssize;
Canvas.addEventListener("mousemove", event => {

    Mouse.x = event.offsetX;
    Mouse.y = event.offsetY;

    range = document.getElementById("fontsize");
    // console.log(Mouse.x, Mouse.y);
    // console.log(fontsize.value + "QWQ");
    T_range = range.value;
    document.getElementById("n_fontsize").value = T_range;

    // F_size = (C_range > T_range) ? C_range : T_range;
    ctx.lineWidth = T_range;
    // console.log(ctx.lineWidth + "Q");
    // console.log(Drawmode);
    if (Drawmode == Pen) {
        if (StartDraw == 1) {
            document.getElementById("canvas").className = "pen_";

            // document.getElementById("pen").style.cursor = "pointer";

            ctx.beginPath();
            ctx.moveTo(Lastmouse.x, Lastmouse.y);
            ctx.lineTo(Mouse.x, Mouse.y);
            ctx.stroke();
            // ctx.closePath();
            Lastmouse.x = Mouse.x;
            Lastmouse.y = Mouse.y;
            ctx.closePath();
        }
    } else if (Drawmode == Eraser) {
        if (StartDraw == 1) {

            ctx.lineWidth = F_size + 0;
            // console.log(ctx.lineWidth);

            ctx.globalCompositeOperation = "destination-out";
            ctx.beginPath();
            ctx.moveTo(Lastmouse.x, Lastmouse.y);
            ctx.lineTo(Mouse.x, Mouse.y);
            ctx.stroke();
            Lastmouse.x = Mouse.x;
            Lastmouse.y = Mouse.y;
            ctx.closePath();
            ctx.lineWidth = fontsize.value;
        }
    } else if (Drawmode == Circle) {
        if (StartDraw == 1) {
            deleteShape();
            ctx.beginPath();
            ctx.arc(Lastmouse.x, Lastmouse.y, Math.abs(Lastmouse.x - Mouse.x), 0, Math.PI * 2);
            ctx.stroke();
            ctx.closePath();

        }
    } else if (Drawmode == Rectangle) {
        if (StartDraw == 1) {
            deleteShape();
            ctx.beginPath();
            ctx.strokeRect(Lastmouse.x, Lastmouse.y, Mouse.x - Lastmouse.x, Mouse.y - Lastmouse.y, Math.PI * 2);
            ctx.stroke();
            ctx.closePath();

        }
    } else if (Drawmode == Triangle) {
        if (StartDraw == 1) {
            deleteShape();
            var line = (Mouse.x - Lastmouse.x);
            ctx.beginPath();
            ctx.moveTo(Lastmouse.x, Lastmouse.y);
            ctx.lineTo(Mouse.x, Mouse.y);
            ctx.lineTo(2 * Lastmouse.x - Mouse.x, Mouse.y);
            ctx.moveTo(2 * Lastmouse.x - Mouse.x, Mouse.y);
            ctx.lineTo(Lastmouse.x, Lastmouse.y);

            // console.log(line);
            ctx.stroke();
            ctx.closePath();
        }
    } else if (Drawmode == Word) {
        if (StartDraw == 1) {
            deleteShape();
            ctx.font = ssize + "px " + ffont;

            var innertext = document.getElementById("TEXT").value;
            // ctx.font = "30px Arial";
            ctx.fillText(innertext, Lastmouse.x, Lastmouse.y);
            // var fontselect = document.getElementById("fonts");
            // var index_ = fontselect.selectedIndex;
            // var val = fontselect.options[index_].text;

            // var fontsize1 = document.getElementById("size");
            // var index_1 = fontsize1.selectedIndex;
            // var val1 = fontsize1.options[index_1].text;

            // ctx.font = val1 + val;
        }

    }

});


var upl = document.getElementById("label");
upl.addEventListener("change", event => {
    var tmpimg = new Image();
    tmpimg.src = Canvas.toDataURL();
    redo_history.push(tmpimg);
    // var img = ctx.getImageData(0, 0, Canvas.width, Canvas.height);
    var File = event.target.files[0];
    var read = new FileReader();
    read.readAsDataURL(File);
    read.onloadend = function(event) {
        var tmp = new Image();
        tmp.src = event.target.result;
        tmp.onload = function(event) {
            ctx.drawImage(tmp, 0, 0, Canvas.width, Canvas.height);
        }
    }


})
Canvas.addEventListener("mousedown", event => {
    var tmpimg = new Image();
    tmpimg.src = Canvas.toDataURL();
    redo_history.push(tmpimg);
    ctx.strokeStyle = document.getElementById("colorpicker").value;
    StartDraw = 1;
    Lastmouse.x = event.offsetX;
    Lastmouse.y = event.offsetY;
    // var ctx = Canvas.getContext("2d");
    // ctx.lineTo(mouse.x, mouse.y);
    Lastcanvas.src = Canvas.toDataURL();

    if (Drawmode == Word) {
        ctx.font = ssize + "px " + ffont;
        ctx.lineWidth = T_range;
        // console.log(ctx.lineWidth);
        var innertext = document.getElementById("TEXT").value;
        // ctx.font = "30px Arial";
        ctx.fillText(innertext, Lastmouse.x, Lastmouse.y);
        // var fontselect = document.getElementById("fonts");
        // var index_ = fontselect.selectedIndex;
        // var val = fontselect.options[index_].text;

        // var fontsize1 = document.getElementById("size");
        // var index_1 = fontsize1.selectedIndex;
        // var val1 = fontsize1.options[index_1].text;

        // ctx.font = val1 + val;


    }

})
Canvas.addEventListener("mouseup", event => {
    StartDraw = 0;
    if (Drawmode == Eraser) {
        ctx.globalCompositeOperation = "source-over";
    }
    // lastmouse.x = 0;
    // lastmouse.y = 0;
    // var ctx = Canvas.getContext("2d");
    // ctx.lineTo(mouse.x, mouse.y);
})

Canvas.addEventListener("mouseout", event => {
    StartDraw = 0;
    if (Drawmode == Eraser) {
        ctx.globalCompositeOperation = "source-over";
    }

})



function reset() {
    document.getElementById("canvas").className = "reset_";
    ctx.clearRect(0, 0, Canvas.width, Canvas.height);
    // console.log("Clear!");
    redo_history = [];
    undo_history = [];
    Drawmode = Clear;
    Lastcanvas.src = Canvas.toDataURL();

}

function eraser() {
    document.getElementById("canvas").className = "eraser_";
    // console.log("Eraser!");
    Drawmode = Eraser;
}

function pen() {
    document.getElementById("canvas").className = "pen_";
    // console.log("Pen!");
    Drawmode = Pen;
}

function triangle() {
    document.getElementById("canvas").className = "triangle_";

    // console.log("Triangle!");
    Drawmode = Triangle;
}

function rectangle() {
    document.getElementById("canvas").className = "rectangle_";
    // console.log("Rectangle!");
    Drawmode = Rectangle;
}

function circle() {
    document.getElementById("canvas").className = "circle_";
    // console.log("Circle!");
    Drawmode = Circle;
}

function word() {
    document.getElementById("canvas").className = "text_";

    // console.log("Text!");
    Drawmode = Word;

}

function deleteShape() {
    ctx.clearRect(0, 0, Canvas.width, Canvas.height);
    ctx.drawImage(Lastcanvas, 0, 0);
}

function undo() {
    if (redo_history[redo_history.length - 1]) {
        var tmpimg = new Image();
        tmpimg.src = Canvas.toDataURL();
        undo_history.push(tmpimg);
        // console.log("JJJJJJJJ");
        ctx.clearRect(0, 0, Canvas.width, Canvas.height);
        ctx.drawImage(redo_history[redo_history.length - 1], 0, 0);
        // console.log("AAAAAAAAA");

        // ㄍundo_history.push(redo_history[redo_history.length - 1]);
        redo_history.pop();
        // console.log("UNDO_POP&PUSH");
        console.log("\n");
    } else {
        console.log("no if");
    }


}

function redo() {
    if (undo_history[undo_history.length - 1]) {
        var tmpimg = new Image();
        tmpimg.src = Canvas.toDataURL();
        redo_history.push(tmpimg);
        // console.log("load undo image");
        ctx.clearRect(0, 0, Canvas.width, Canvas.height);
        ctx.drawImage(undo_history[undo_history.length - 1], 0, 0);
        // console.log("QQQQQQ");
        //redo_history.push(undo_history[undo_history.length - 1]);
        undo_history.pop();
        // console.log("REDO_POP&PUSH");
    } else {
        console.log("no if");
    }

}


function download() {
    var img = Canvas.toDataURL("image/png");
    var image = document.getElementById("Download");
    image.href = img;
}

function Font(value) {
    ffont = value;
}

function Size(value) {
    ssize = value;
}